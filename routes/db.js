var mongo = require('mongodb');
var url = "mongodb://localhost:27017/FlipZone";
var mongoose = require('mongoose');

mongoose.connect(url,{});
var db = mongoose.connection;

module.exports = function(app){

	//SignUp
    app.get('/SignUp',function(req, res){

	  db.collection("Users").insert({
          Name: req.query.Name,
          Email: req.query.Email,
          Username: req.query.Username,
          Password: req.query.Password
      }, function(err, res) {
	    if (err) throw err;
	    console.log("Number of documents inserted: " + res.insertedCount);
	    db.close();
	  });
        res.redirect("../");
    });

    //SignIn
    app.get('/SignIn',function(req,res){

    	db.collection("Users").find({
            Username: req.query.Username,
            Password: req.query.Password
        }, function(err, res){
            if (err) throw err;
            console.log("User Found");
            db.close();
		});
        res.redirect("../");
	});

    //NewsLetter
    app.get('/NewsLetter', function(req,res){
    	db.collection("Subscribers").insert({Email : req.query.Email}, function(err,res){
            if (err) throw err;
            console.log("One User Subscribed");
            db.close();
		});
    	res.redirect("../");
	});
}    

