var http = require('http');
var express = require('express');
var app = express();


app.use(express.static('public'));

require('./routes/db.js')(app);
require ('./routes/router.js')(app);

var server = app.listen(8099, function(err,res){
    if(err){
        console.log("error in creating server");
    }
    else{
        console.log("Server created at "+ server.address().port);
    }
})
